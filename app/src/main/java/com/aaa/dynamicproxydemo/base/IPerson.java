package com.aaa.dynamicproxydemo.base;

/**
 * 定义一个IPerson接口，有一个获取姓名的方法
 */
public interface IPerson {
    String getName();
}
