package com.aaa.dynamicproxydemo.base;

/**
 * 被代理的实际类
 */
public class RealPerson implements IPerson {
    @Override
    public String getName() {
        System.out.println("被代理的实际类");
        return "被代理的实际类";
    }
}
