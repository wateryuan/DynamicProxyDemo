package com.aaa.dynamicproxydemo.dynamic_proxy;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.aaa.dynamicproxydemo.R;
import com.aaa.dynamicproxydemo.base.IPerson;
import com.aaa.dynamicproxydemo.base.RealPerson;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dynamicTest();
    }

    public void dynamicTest() {
        DynamicProxy dynamicProxy = new DynamicProxy(new RealPerson());
        IPerson iPerson = (IPerson) dynamicProxy.newProxyInstance();
        iPerson.getName();

        /*
2019-10-23 13:47:09.869 26209-26209/com.aaa.dynamicproxydemo I/System.out: do before dynamic Proxy
2019-10-23 13:47:09.869 26209-26209/com.aaa.dynamicproxydemo I/System.out: 被代理的实际类
2019-10-23 13:47:09.870 26209-26209/com.aaa.dynamicproxydemo I/System.out: do after dynamic Proxy
         */
    }
}
