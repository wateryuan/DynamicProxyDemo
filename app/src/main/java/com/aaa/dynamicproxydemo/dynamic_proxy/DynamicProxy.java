package com.aaa.dynamicproxydemo.dynamic_proxy;

import android.util.Log;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 不涉及具体的接口及类型，所以可代理多个不同接口及类型
 */
public class DynamicProxy implements InvocationHandler {
    private static final String TAG = "DynamicProxy";
    private Object mObject;

    public DynamicProxy(Object object) {
        Log.i(TAG, "DynamicProxy: 不涉及具体的接口及类型，所以可同时代理多个不同接口及类型");
        mObject = object;
    }

    public Object newProxyInstance() {
        return Proxy.newProxyInstance(mObject.getClass().getClassLoader(), mObject.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("do before dynamic Proxy");
        Object invoke = method.invoke(mObject, args);
        System.out.println("do after dynamic Proxy");
        return invoke;
    }
}
