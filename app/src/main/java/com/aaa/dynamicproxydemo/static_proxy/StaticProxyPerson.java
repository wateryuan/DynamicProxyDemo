package com.aaa.dynamicproxydemo.static_proxy;

import com.aaa.dynamicproxydemo.base.IPerson;

/**
 * 静态代理要实现具体接口
 */
public class StaticProxyPerson implements IPerson {

    private IPerson mPerson;

    public StaticProxyPerson(IPerson iPerson) {
        System.out.println("静态代理：涉及具体的接口类型，不同接口要有不同的代理类");
        mPerson = iPerson;
    }

    @Override
    public String getName() {
        System.out.println("doBefore proxy");
        mPerson.getName();
        System.out.println("doAfter proxy");
        return "静态代理";
        /**
         静态代理
         doBefore proxy
         被代理的实际类
         doAfter proxy
         * */
    }
}
