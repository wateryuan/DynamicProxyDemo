package com.aaa.dynamicproxydemo.static_proxy;

import com.aaa.dynamicproxydemo.base.RealPerson;

public class StaticProxyTest {
    public static void main(String[] args) {
        StaticProxyPerson proxyPerson = new StaticProxyPerson(new RealPerson());
        proxyPerson.getName();
    }
}
