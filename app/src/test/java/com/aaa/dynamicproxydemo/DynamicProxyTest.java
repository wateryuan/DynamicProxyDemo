package com.aaa.dynamicproxydemo;

import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class DynamicProxyTest {

    public interface MyRequest {
        @GET("ajax.php?a=fy&f=auto&t=auto&w=")
        Observable<String> getTranslation(@Query("w") String string);
    }

    public class MyRetrofit {
        public MyRetrofit() {
        }

        public <T> T create(Class<T> services) {
            return (T) Proxy.newProxyInstance(services.getClassLoader(), new Class[]{services}, new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    System.out.println("方法名=" + method.getName());

                    Annotation[] annotations = method.getAnnotations();
                    for (Annotation annotation : annotations) {
                        System.out.println("方法的注解=" + annotation);

                        // TODO: 2019/11/22  
                        System.out.println(annotation.annotationType());
                    }

                    Annotation[][] parameterAnnotations = method.getParameterAnnotations();
                    for (Annotation[] parameterAnnotation : parameterAnnotations) {
                        for (Annotation annotation : parameterAnnotation) {
                            System.out.println("方法的参数的注解=" + annotation);
                        }
                    }

                    System.out.println("方法的参数值=" + Arrays.toString(args));
                    return null;
                }
            });
        }
    }


    @Test
    public void test() {
        /*
test
方法名=getTranslation
方法的注解=@retrofit2.http.GET(value=ajax.php?a=fy&f=auto&t=auto&w=)
方法的参数的注解=[@retrofit2.http.Query(encoded=false, value=w)]
方法的参数值=[hello]
         * */
        MyRequest myRequest = new MyRetrofit().create(MyRequest.class);
        System.out.println("test");
        myRequest.getTranslation("hello");
    }
}
